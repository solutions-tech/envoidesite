<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class AppController extends Controller
{
    public function app()
    {
        return Inertia::render('Dashboard');
    }


    public function login()
    {
        return Inertia::render('Login/Index');
    }

    public function postlogin(Request $request)
    {
        if (Auth::attempt(['login' => $request->login, 'password' => $request->password, 'active' => 1])) {
            return redirect()->route('dashboard')->with('success', 'Connection avec succès');
        } else {
            return back()->with('error', 'Information de connection incorrecte');
        }
    }

    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect()->route('login');
    }
}
