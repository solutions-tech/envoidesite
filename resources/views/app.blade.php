<!DOCTYPE html>
<html>

<head>
    <link href="{{ mix('/css/app.css') }}" rel="stylesheet" />
    <script src="{{ mix('/js/app.js') }}" defer></script>

    <title>Twins Immobilier</title>
    <!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 10]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />
    <!-- Favicon icon -->
    <link rel="icon" href={{asset("assets/images/favicon.ico")}} type="image/x-icon">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500" rel="stylesheet">
    <!-- waves.css -->
    <link rel="stylesheet" href={{asset("assets/pages/waves/css/waves.min.css")}} type="text/css" media="all">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href={{asset("assets/css/bootstrap/css/bootstrap.min.css")}}>
    <!-- waves.css -->
    <link rel="stylesheet" href={{asset("assets/pages/waves/css/waves.min.css")}} type="text/css" media="all">
    <!-- themify icon -->
    <link rel="stylesheet" type="text/css" href={{asset("assets/icon/themify-icons/themify-icons.css")}}>
    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href={{asset("assets/icon/font-awesome/css/font-awesome.min.css")}}>
    <!-- scrollbar.css -->
    <link rel="stylesheet" type="text/css" href={{asset("assets/css/jquery.mCustomScrollbar.css")}}>
    <!-- am chart export.css -->
    <link rel="stylesheet" href={{asset("https://www.amcharts.com/lib/3/plugins/export/export.css")}} type="text/css" media="all" />
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href={{asset("assets/css/style.css")}}>

</head>

<body>
    @routes
    @inertia


    <!-- Warning Section Starts -->
    <!-- Older IE warning message -->
    <!--[if lt IE 10]>
    <div class="ie-warning">
        <h1>Warning!!</h1>
        <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
        <div class="iew-container">
            <ul class="iew-download">
                <li>
                    <a href="http://www.google.com/chrome/">
                        <img src="assets/images/browser/chrome.png" alt="Chrome">
                        <div>Chrome</div>
                    </a>
                </li>
                <li>
                    <a href="https://www.mozilla.org/en-US/firefox/new/">
                        <img src="assets/images/browser/firefox.png" alt="Firefox">
                        <div>Firefox</div>
                    </a>
                </li>
                <li>
                    <a href="http://www.opera.com">
                        <img src="assets/images/browser/opera.png" alt="Opera">
                        <div>Opera</div>
                    </a>
                </li>
                <li>
                    <a href="https://www.apple.com/safari/">
                        <img src="assets/images/browser/safari.png" alt="Safari">
                        <div>Safari</div>
                    </a>
                </li>
                <li>
                    <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                        <img src="assets/images/browser/ie.png" alt="">
                        <div>IE (9 & above)</div>
                    </a>
                </li>
            </ul>
        </div>
        <p>Sorry for the inconvenience!</p>
    </div>
    <![endif]-->
    <!-- Warning Section Ends -->

    <!-- Required Jquery -->
    <script type="text/javascript" src={{asset("assets/js/jquery/jquery.min.js")}}></script>
    <script type="text/javascript" src={{asset("assets/js/jquery-ui/jquery-ui.min.js")}}></script>
    <script type="text/javascript" src={{asset("assets/js/popper.js/popper.min.js")}}></script>
    <script type="text/javascript" src={{asset("assets/js/bootstrap/js/bootstrap.min.js")}}></script>
    <script type="text/javascript" src={{asset("assets/pages/widget/excanvas.js")}}></script>
    <!-- waves js -->
    <script src={{asset("assets/pages/waves/js/waves.min.js")}}></script>
    <!-- jquery slimscroll js -->
    <script type="text/javascript" src={{asset("assets/js/jquery-slimscroll/jquery.slimscroll.js")}}></script>
    <!-- modernizr js -->
    <script type="text/javascript" src={{asset("assets/js/modernizr/modernizr.js")}}></script>
    <!-- slimscroll js -->
    <script type="text/javascript" src={{asset("assets/js/SmoothScroll.js")}}></script>
    <script src={{asset("assets/js/jquery.mCustomScrollbar.concat.min.js")}}></script>
    <!-- Chart js -->
    <script type="text/javascript" src={{asset("assets/js/chart.js/Chart.js")}}></script>
    <!-- amchart js -->
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
    <script src={{asset("assets/pages/widget/amchart/gauge.js")}}></script>
    <script src={{asset("assets/pages/widget/amchart/serial.js")}}></script>
    <script src={{asset("assets/pages/widget/amchart/light.js")}}></script>
    <script src={{asset("assets/pages/widget/amchart/pie.min.js")}}></script>
    <script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
    <!-- menu js -->
    <script src={{asset("assets/js/pcoded.min.js")}}></script>
    <script src={{asset("assets/js/vertical-layout.min.js")}}></script>
    <!-- custom js -->
    <script type="text/javascript" src={{asset("assets/pages/dashboard/custom-dashboard.js")}}></script>
    <script type="text/javascript" src={{asset("assets/js/script.js")}}></script>
</body>
</body>

</html>