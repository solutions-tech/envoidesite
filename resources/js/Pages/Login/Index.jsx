import { Inertia } from '@inertiajs/inertia';
import React, { useState } from 'react';

const Index = ({ flash }) => {
    const [values, setValues] = useState({
        login: '',
        password: '',
    });

    const handleChange = (e) => {
        e.persist()
        setValues((values) => ({ ...values, [e.target.name]: e.target.value }))
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        Inertia.post(route('postlogin'), values, {
            onSuccess: () => flash.error == null ? window.location.href = Ziggy.url + '/core/dashboard' : null
        })
    }

    return (<>
        <div className="theme-loader">
            <div className="loader-track">
                <div className="preloader-wrapper">
                    <div className="spinner-layer spinner-blue">
                        <div className="circle-clipper left">
                            <div className="circle" />
                        </div>
                        <div className="gap-patch">
                            <div className="circle" />
                        </div>
                        <div className="circle-clipper right">
                            <div className="circle" />
                        </div>
                    </div>
                    <div className="spinner-layer spinner-red">
                        <div className="circle-clipper left">
                            <div className="circle" />
                        </div>
                        <div className="gap-patch">
                            <div className="circle" />
                        </div>
                        <div className="circle-clipper right">
                            <div className="circle" />
                        </div>
                    </div>
                    <div className="spinner-layer spinner-yellow">
                        <div className="circle-clipper left">
                            <div className="circle" />
                        </div>
                        <div className="gap-patch">
                            <div className="circle" />
                        </div>
                        <div className="circle-clipper right">
                            <div className="circle" />
                        </div>
                    </div>
                    <div className="spinner-layer spinner-green">
                        <div className="circle-clipper left">
                            <div className="circle" />
                        </div>
                        <div className="gap-patch">
                            <div className="circle" />
                        </div>
                        <div className="circle-clipper right">
                            <div className="circle" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {/* Pre-loader end */}
        <section className="login-block">
            {/* Container-fluid starts */}
            <div className="container">
                <div className="row">
                    <div className="col-sm-12">
                        {/* Authentication card start */}
                        <form className="md-float-material form-material" onSubmit={handleSubmit}>
                            <div className="text-center">
                                <img src="assets/images/logo.png" alt="logo.png" />
                            </div>
                            {flash.error && <div class="alert alert-danger text-center" role="alert">
                                {flash.error}
                            </div>}
                            <div className="auth-box card">
                                <div className="card-block">
                                    <div className="row m-b-20">
                                        <div className="col-md-12">
                                            <h3 className="text-center">Sign In</h3>
                                        </div>
                                    </div>
                                    <div className="form-group form-primary">
                                        <input type="text" name="login" className="form-control" required onChange={handleChange} />
                                        <span className="form-bar" />
                                        <label className="float-label">Login</label>
                                    </div>
                                    <div className="form-group form-primary">
                                        <input type="password" name="password" className="form-control" required onChange={handleChange} />
                                        <span className="form-bar" />
                                        <label className="float-label">Password</label>
                                    </div>
                                    <div className="row m-t-25 text-left">
                                        <div className="col-12">
                                            <div className="checkbox-fade fade-in-primary d-">
                                                {/* <label>
                                                    <input type="checkbox" checked={values.remember ? true : false} onChange={(e) => setValues({ ...values, remember: e.target.checked })} />
                                                    <span className="cr"><i className="cr-icon icofont icofont-ui-check txt-primary" /></span>
                                                    <span className="text-inverse">Remember me</span>
                                                </label> */}
                                            </div>
                                            <div className="forgot-phone text-right f-right">
                                                <a href="#" className="text-right f-w-600"> Forgot Password?</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row m-t-30">
                                        <div className="col-md-12">
                                            <button type="submit" className="btn btn-primary btn-md btn-block waves-effect waves-light text-center m-b-20">Se Connecter</button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </form>
                        {/* end of form */}
                    </div>
                    {/* end of col-sm-12 */}
                </div>
                {/* end of row */}
            </div>
            {/* end of container-fluid */}
        </section>

    </>
    );
}

export default Index;
