import React from 'react';
import LeftBar from '../../shared/LeftBar';
import TopBar from '../../shared/TopBar';

const Backend = ({ children }) => {
    return (
        <>
            <div className="theme-loader">
                <div className="loader-track">
                    <div className="preloader-wrapper">
                        <div className="spinner-layer spinner-blue">
                            <div className="circle-clipper left">
                                <div className="circle" />
                            </div>
                            <div className="gap-patch">
                                <div className="circle" />
                            </div>
                            <div className="circle-clipper right">
                                <div className="circle" />
                            </div>
                        </div>
                        <div className="spinner-layer spinner-red">
                            <div className="circle-clipper left">
                                <div className="circle" />
                            </div>
                            <div className="gap-patch">
                                <div className="circle" />
                            </div>
                            <div className="circle-clipper right">
                                <div className="circle" />
                            </div>
                        </div>
                        <div className="spinner-layer spinner-yellow">
                            <div className="circle-clipper left">
                                <div className="circle" />
                            </div>
                            <div className="gap-patch">
                                <div className="circle" />
                            </div>
                            <div className="circle-clipper right">
                                <div className="circle" />
                            </div>
                        </div>
                        <div className="spinner-layer spinner-green">
                            <div className="circle-clipper left">
                                <div className="circle" />
                            </div>
                            <div className="gap-patch">
                                <div className="circle" />
                            </div>
                            <div className="circle-clipper right">
                                <div className="circle" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="pcoded" className="pcoded">
                <div className="pcoded-overlay-box" />
                <div className="pcoded-container navbar-wrapper">
                    {/* top bar */}
                    <TopBar />
                    <div className="pcoded-main-container">
                        <div className="pcoded-wrapper">
                            {/* left bar */}
                            <LeftBar />
                            <div className="pcoded-content">
                                {/* Page-header start */}
                                <div className="page-header">
                                    <div className="page-block">
                                        <div className="row align-items-center">
                                            <div className="col-md-8">
                                                <div className="page-header-title">
                                                    <h5 className="m-b-10">Dashboard</h5>
                                                    <p className="m-b-0">Welcome to Mega Able</p>
                                                </div>
                                            </div>
                                            <div className="col-md-4">
                                                <ul className="breadcrumb-title">
                                                    <li className="breadcrumb-item">
                                                        <a href="index.html"> <i className="fa fa-home" /> </a>
                                                    </li>
                                                    <li className="breadcrumb-item"><a href="#!">Dashboard</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {/* Page-header end */}
                                <div className="pcoded-inner-content">
                                    {/* Main-body start */}
                                    {children}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </>

    );
}

export default Backend;
