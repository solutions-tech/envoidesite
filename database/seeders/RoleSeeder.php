<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create(['name' => 'admin']);
        Role::create(['name' => 'direction commercial']);
        Role::create(['name' => 'commercial']);
        Role::create(['name' => 'proprietaire']);
        Role::create(['name' => 'locataire']);
        Role::create(['name' => 'acquereur de bien']);
        Role::create(['name' => 'vendeur de bien']);
    }
}
