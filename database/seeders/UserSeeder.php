<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'firstname' => 'Koffi',
            'lastname' => 'Prudence',
            'role_id' => 1,
            'active' => 1,
            'email' => 'admin@gmail.com',
            'login' => 'admin',
            'password' => Hash::make('password')
        ]);
    }
}
