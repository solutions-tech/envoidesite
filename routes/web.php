<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AppController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [AppController::class, 'login'])->name('login');
Route::post('/postlogin', [AppController::class, 'postlogin'])->name('postlogin');
Route::get('/logout', [AppController::class, 'logout'])->name('logout');

Route::group(['prefix' => 'core', 'middleware' => 'auth'], function () {
    Route::get('/dashboard', [AppController::class, 'app'])->name('dashboard');
});
